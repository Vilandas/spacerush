
/**
 * Performs a single (static) draw for the parent sprite
 * @author
 * @version 1.0
 * @class SpriteArtist
 */
class SpriteArtist 
{
    constructor(context, spritesheet, 
        sourcePosition, sourceDimensions)
    {
        this.context = context;
        this.spritesheet = spritesheet;
        this.sourcePosition = sourcePosition;
        this.sourceDimensions = sourceDimensions;
    }

    Update(gameTime, parent)
    {

    }

    Draw(gameTime, parent)
    {
        this.context.save();
        var transform = parent.Transform2D;
        this.context.translate(
            transform.TranslationOffset.X, 
            transform.TranslationOffset.Y);
        this.context.scale(transform.Scale.X, transform.Scale.Y);

        this.context.drawImage(this.spritesheet, 
            this.sourcePosition.X, this.sourcePosition.Y, 
            this.sourceDimensions.X, this.sourceDimensions.Y, 
            transform.Translation.X, transform.Translation.Y, //0, 0
            transform.Dimensions.X, transform.Dimensions.Y);
        this.context.restore();
    }

    //to do...Equals()

    Clone()
    {
        return new SpriteArtist(this.context, this.spritesheet, this.sourcePosition.Clone(), this.sourceDimensions.Clone());
    }

    ToString()
    {
        return "[" + this.spritesheet + "," + this.sourcePosition.ToString() + "," + this.sourceDimensions.ToString() +"]";
    }
}