
/**
 * Performs a single (static) draw of a rectangle primitive for the parent sprite
 * @author
 * @version 1.0
 * @class RectangleSpriteArtist
 */
class RectangleSpriteArtist 
{
    constructor(context, rect, lineWidth, strokeStyle, 
        fillStyle, alpha)
    {
        this.context = context;
        this.rect = rect;
        this.lineWidth = lineWidth;
        this.strokeStyle = strokeStyle;
        this.fillStyle = fillStyle;
        this.alpha = alpha;
    }

    Update(gameTime, parent)
    {

    }

    Draw(gameTime, parent)
    {
        this.context.save();
        var transform = parent.Transform2D;
        this.context.translate(
            transform.TranslationOffset.X, 
            transform.TranslationOffset.Y);
        this.context.scale(transform.Scale.X, transform.Scale.Y);

        this.context.lineWidth = this.lineWidth;
        this.context.strokeStyle = this.strokeStyle;
        this.context.fillStyle = this.fillStyle;
        this.context.globalAlpha = this.alpha;
        this.context.strokeRect(this.rect.X, this.rect.Y, this.rect.Width, this.rect.Height);
        this.context.fillRect(this.rect.X, this.rect.Y, this.rect.Width, this.rect.Height);

        this.context.restore();
    }

    //to do...Equals()

    Clone()
    {
        return new RectangleSpriteArtist(this.context, this.rect.Clone(), this.lineWidth, this.strokeStyle, this.fillStyle, this.alpha);
    }

    ToString()
    {
        return "[" + this.rect + "," + this.lineWidth + "," + this.strokeStyle + "," + this.fillStyle + "," + this.alpha +"]";
    }
}