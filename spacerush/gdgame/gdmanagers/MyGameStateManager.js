/**
 * Stores, updates, and changes game state in my specific game e.g. Snailbait.
 * @author
 * @version 1.0
 * @class MyGameStateManager
 */

class MyGameStateManager extends GameStateManager 
{
  constructor(id, notificationCenter, gameTime) {
      super(id);

      this.notificationCenter = notificationCenter;
      this.RegisterForNotifications();
      this.health = {};
      this.score = 0;
      this.bonusScore = 0;
      this.timer = new Stopwatch();
  }

  get Score() { return this.score; }
  get Timer() { return this.timer; }
  get PlayerHealth() { return this.health["player"]; }

  //handle all GameState type events - see PlayerBehavior::HandleEnemyCollision()
  RegisterForNotifications() {
      this.notificationCenter.Register(NotificationType.GameState, this, this.HandleNotification);
      this.notificationCenter.Register(NotificationType.Time, this, this.HandleTimeNotifaction);
  }

  HandleNotification(...argArray)
  {
    let notification = argArray[0];
    switch(notification.NotificationAction)
    {
      case NotificationAction.Health:
        this.HandleHealthStateChange(notification.NotificationArguments);  
        break;

      case NotificationAction.Inventory:
        this.HandleInventoryStateChange(notification.NotificationArguments);  
        break;

      case NotificationAction.Ammo:
        this.HandleAmmoStateChange(notification.NotificationArguments);  
        break;
      
      case NotificationAction.Spawn:
        this.HandleSpawn(notification.NotificationArguments);  
        break;

      case NotificationAction.Despawn:
        this.HandleDespawn(notification.NotificationArguments);  
        break;
      default:
        break;  
    }
  }

  HandleTimeNotifaction(...argArray)
  {
    let notification = argArray[0];
    switch(notification.NotificationAction)
    {
      case NotificationAction.Start:
        this.StartTimer();
        break;
      case NotificationAction.Pause:
        this.PauseTimer();
        break;
      case NotificationAction.Resume:
        this.ResumeTimer();
        break;
        case NotificationAction.Reset:
          this.ResetTimer();
          break;
    }
  }
    
  HandleHealthStateChange(argArray){
    this.health[argArray[0].ID] += argArray[1];
    if(this.health[argArray[0].ID] <= 0)
    {
      if(argArray[0] instanceof Asteroid)
        this.bonusScore += 50;
      else if(argArray[0] instanceof SmallShip)
        this.bonusScore += 100;
      else if(argArray[0] instanceof MidShip)
        this.bonusScore += 200;
      else if(argArray[0] instanceof BigShip)
        this.bonusScore += 400;
      else if(argArray[0] instanceof Player)
        this.StoreScore();

      this.notificationCenter.Notify(new Notification(NotificationType.Sprite, 
        NotificationAction.Remove, [argArray[0]]));
    }
  }

  HandleInventoryStateChange(...argArray){
    console.log(argArray);
    //add your code here..maybe update a UI, or a health variable?
  }

  HandleAmmoStateChange(...argArray){
    console.log(argArray);
    //add your code here..maybe update a UI, or a health variable?
  }

  HandleSpawn(argArray){
    this.health[argArray[0].ID] = argArray[1];
  }

  HandleDespawn(argArray){
    delete this.health[argArray[0].ID];
  }

  StartTimer()
  {
    this.timer.Start();
  }

  PauseTimer()
  {
    this.timer.Pause();
  }

  ResumeTimer()
  {
    this.timer.Unpause();
  }

  ResetTimer()
  {
    //this.timer.Reset();
  }


  Update() 
  {
    if(!this.timer.IsPaused)
    {
      this.score =  Math.round(this.timer.GetElapsedTime() / 100) + this.bonusScore;
    }
  }

  StoreScore()
  {
    var name = "Highscore";
    var data = parseInt(document.getElementById("highscore").innerHTML, 10);

    if(this.score > data)
    {
      localStorage.setItem(name, this.score);
    }
  }

}