class Explosion extends Sprite
{
    constructor(ctx, spriteSheet, objectManager, position)
    {
        super(("Explosion - " + objectManager.Explosion),
            ActorType.Decorator,
            new Transform2D(
                new Vector2(
                    position.X - (EXPLOSION_CELLS_WIDTH/2),
                    position.Y
                ),
                0,
                new Vector2(1, 1),
                new Vector2(0, 0),
                new Vector2(EXPLOSION_CELLS_WIDTH, EXPLOSION_CELLS_HEIGHT)
            ),
            new AnimatedSpriteArtist(
                ctx,
                spriteSheet,
                EXPLOSION_ANIMATION_FPS,
                EXPLOSION_CELLS,
                0
            ),
            StatusType.IsUpdated | StatusType.IsDrawn,
            new Vector2(0, 0),
            0
        );
        objectManager.Add(this);
    }
}