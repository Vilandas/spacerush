class SmallShip extends Sprite
{
    constructor(ctx, SpriteSheet, objectManager)
    {
        super(("SmallShip - " + objectManager.SmallShipCount),
            ActorType.Enemy,
            new Transform2D(
                new Vector2(
                    //Translate random X value of canvas width, and above the screen for Y
                    Math.random() * (cvs.clientWidth - SMALL_SHIP_CELLS_WIDTH), -SMALL_SHIP_CELLS_HEIGHT
                ),
                0,
                new Vector2(1, 1),
                new Vector2(0, 0),
                new Vector2(SMALL_SHIP_CELLS[0].width, SMALL_SHIP_CELLS_HEIGHT)
            ),
            new AnimatedSpriteArtist(
                ctx,
                SpriteSheet,
                SMALL_SHIP_ANIMATION_FPS,
                SMALL_SHIP_CELLS,
                Math.floor(Math.random() * ASTEROID_CELLS.length) //Random start cells
            ),
            StatusType.IsUpdated | StatusType.IsDrawn,
            new Vector2(0, SMALL_SHIP_BASE_FALL_SPEED),
            0
        );
        
        objectManager.Add(this);
    }
}