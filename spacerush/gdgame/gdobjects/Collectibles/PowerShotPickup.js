class PowerShotPickup extends Sprite
{
    constructor(ctx, spriteSheet, objectManager)
    {
        super("PowerShotPickup - " + objectManager.PowerShotCount,
            ActorType.Interactable,
            new Transform2D(
                new Vector2(
                Math.random() * (cvs.clientWidth - POWERSHOT_PICKUP_CELLS_WIDTH), -POWERSHOT_PICKUP_CELLS_HEIGHT
            ),
            0,
                new Vector2(1,1),
                new Vector2(0,0),
                new Vector2(POWERSHOT_PICKUP_CELLS[0].width, POWERSHOT_PICKUP_CELLS_HEIGHT)
            ),
            new AnimatedSpriteArtist(
                ctx,
                spriteSheet,
                POWERSHOT_PICKUP_ANIMATION_FPS, 
                POWERSHOT_PICKUP_CELLS,
                Math.floor(Math.random() * POWERSHOT_PICKUP_CELLS.length)
            ),       
            StatusType.IsUpdated | StatusType.IsDrawn,
            new Vector2(0, POWERSHOT_PICKUP_BASE_FALL_SPEED),
            0
        );
        objectManager.Add(this);
    }
}