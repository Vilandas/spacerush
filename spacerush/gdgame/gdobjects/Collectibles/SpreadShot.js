class SpreadShot extends Sprite
{
    constructor(ctx, spriteSheet, objectManager)
    {
        super("spreadShot - " + objectManager.SpreadShotCount,
            ActorType.Bullet,
            new Transform2D(
                BULLET_ALL_INITIAL_TRANSLATION,
                0,
                new Vector2(1, 1),
                Vector2.Zero,
                new Vector2(
                    BULLET_SPREADSHOT_WIDTH,
                    BULLET_SPREADSHOT_HEIGHT
                )
            ),
            new AnimatedSpriteArtist(
                ctx,
                spriteSheet,
                BULLET_SPREADSHOT_ANIMATION_FPS,
                BULLET_SPREADSHOT_CELLS,
                0
            ),      
            StatusType.Off
        );

        this.ctx = ctx;
        this.spriteSheet = spriteSheet;
        this.objectManager = objectManager;

        this.AttachBehavior(
            new MoveBehavior(BULLET_SPREADSHOT_DIRECTION, BULLET_SPREADSHOT_MOVE_SPEED)
        );
    }

    Clone() 
    {
        //make a clone of the actor
        let clone = new SpreadShot(this.ctx, this.spriteSheet, this.objectManager);

        //now clone all the actors attached behaviors
        for(let behavior of this.behaviors)
            clone.AttachBehavior(behavior.Clone());
        
        //lastly return the actor
        return clone;
    }
}