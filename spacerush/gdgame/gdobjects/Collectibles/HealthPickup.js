class HealthPickup extends Sprite
{
    constructor(ctx, spriteSheet, objectManager)
    {
        super("HealthPickup - " + objectManager.pickupCount,
            ActorType.Interactable,
            new Transform2D(
                new Vector2(
                Math.random() * (cvs.clientWidth - HEALTH_PICKUP_CELLS_WIDTH), -HEALTH_PICKUP_CELLS_HEIGHT
            ),
            0,
                new Vector2(1,1),
                new Vector2(0,0),
                new Vector2(HEALTH_PICKUP_CELLS[0].width, HEALTH_PICKUP_CELLS_HEIGHT)
            ),
            new AnimatedSpriteArtist(
                ctx,
                spriteSheet,
                HEALTH_PICKUP_ANIMATION_FPS, 
                HEALTH_PICKUP_CELLS,
                Math.floor(Math.random() * HEALTH_PICKUP_CELLS.length)
            ),       
            StatusType.IsUpdated | StatusType.IsDrawn,
            new Vector2(0, HEALTH_PICKUP_BASE_FALL_SPEED),
            0
        );
        objectManager.Add(this);
    }
}