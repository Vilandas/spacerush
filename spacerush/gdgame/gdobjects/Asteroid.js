class Asteroid extends Sprite
{
    constructor(ctx, asteroidSpriteSheet, objectManager)
    {
        super(("asteroid - " + objectManager.AsteroidCount),
            ActorType.Enemy,
            new Transform2D(
                new Vector2(
                    //Translate random X value of canvas width, and above the screen for Y
                    Math.random() * (cvs.clientWidth - ASTEROID_CELLS_WIDTH), -ASTEROID_CELLS_HEIGHT
                ),
                0,
                new Vector2(1, 1),
                new Vector2(0, 0),
                new Vector2(ASTEROID_CELLS_WIDTH, ASTEROID_CELLS_HEIGHT)
            ),
            new AnimatedSpriteArtist(
                ctx,
                asteroidSpriteSheet,
                ASTEROID_ANIMATION_FPS,
                ASTEROID_CELLS,
                Math.floor(Math.random() * ASTEROID_CELLS.length) //Random start cells
            ),
            StatusType.IsUpdated | StatusType.IsDrawn,
            new Vector2(0, ASTEROID_BASE_FALL_SPEED),
            0
        );
        
        objectManager.Add(this);
    }

}