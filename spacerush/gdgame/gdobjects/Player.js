class Player extends MoveableSprite
{
    constructor(ctx, spriteSheet, objectManager, keyboardManager, screenRectangle)
    {
        super("player",
            ActorType.Player,
            new Transform2D(
                new Vector2(PLAYERSHIP_START_X_POSITION, PLAYERSHIP_START_Y_POSITION),
                0,
                new Vector2(1, 1),
                new Vector2(0, 0),
                new Vector2(PLAYERSHIP_CELLS_WIDTH, PLAYERSHIP_CELLS_HEIGHT)  //used for CD/CR rectangle - see MyConstants
            ),
            new AnimatedSpriteArtist(
                ctx,
                spriteSheet,
                PLAYERSHIP_ANIMATION_FPS,
                PLAYERSHIP_CELLS_IDLE,
                0
            ),
            StatusType.IsUpdated | StatusType.IsDrawn
        );
        
        this.AttachBehavior(
            new PlayerBehavior(keyboardManager, objectManager, screenRectangle,
              PLAYERSHIP_HORIZONTAL_VELOCITY, PLAYERSHIP_VERTICAL_VELOCITY,
              PLAYERSHIP_CELLS_IDLE,
              PLAYERSHIP_CELLS_LEFT, PLAYERSHIP_CELLS_RIGHT,
              PLAYERSHIP_CELLS_UP, PLAYERSHIP_CELLS_DOWN,
              BULLET_PLAYER_INTERVAL_IN_MS,
              new Bullet(ctx, spriteSheet, objectManager),
              new PowerShot(ctx, spriteSheet, objectManager),
              new SpreadShot(ctx, spriteSheet, objectManager)
            )
        );

        objectManager.Add(this);
    }

}