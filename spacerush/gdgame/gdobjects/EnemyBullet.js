class EnemyBullet extends Sprite
{
    constructor(ctx, spriteSheet, objectManager)
    {
        super("bullet - " + objectManager.BulletCount,
            ActorType.Bullet,
            new Transform2D(
                BULLET_ALL_INITIAL_TRANSLATION,
                0,
                new Vector2(1, 1),
                Vector2.Zero,
                new Vector2(
                    BULLET_ENEMY_WIDTH,
                    BULLET_ENEMY_HEIGHT
                )
            ),
            new AnimatedSpriteArtist(
                ctx,
                spriteSheet,
                BULLET_ENEMY_ANIMATION_FPS,
                BULLET_ENEMY_CELLS,
                0
            ),
            StatusType.Off
        );
        
        this.ctx = ctx;
        this.spriteSheet = spriteSheet;
        this.objectManager = objectManager;

    }

    Clone() 
    {
        //make a clone of the actor
        let clone = new EnemyBullet(this.ctx, this.spriteSheet, this.objectManager);

        //now clone all the actors attached behaviors
        for(let behavior of this.behaviors)
            clone.AttachBehavior(behavior.Clone());
        
        //lastly return the actor
        return clone;
    }
}