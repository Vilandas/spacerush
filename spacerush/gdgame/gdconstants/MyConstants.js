

//#region Audio data 
//audio - step 2 - create array of cues with same unique IDs as were loaded in HTML file
const audioCueArray = [
	//add game specific audio cues here...
	
	new AudioCue("sound_shoot",
		AudioType.Damage, 1, 1, false, 0),
	new AudioCue("sound_bading",
      AudioType.Damage, 1, 1, false, 0),
   new AudioCue("sound_menu",
      AudioType.Menu, 1, 1, true, 0),
   new AudioCue("sound_game",
      AudioType.Background, 1, 1, true, 0),
   //Old shooting sound if we want to use it for the enemies
   new AudioCue("sound_old_shoot",
      AudioType.Shoot, 1, 1, false, 0),
   new AudioCue("sound_ship_hit",
      AudioType.Damage, 1, 1, false, 0),
   new AudioCue("sound_pickup",
      AudioType.Pickup, 1, 1, false, 0)
   //ship exploding
   // new AudioCue("sound_ship_explode",1,1,false,0),
   // //pickup
   //todo: get sound of playership exploding, get health, multishot and rapidfire sounds
];
//#endregion

//#region PlayerShip


   const PLAYERSHIP_CELLS_WIDTH = 64; // in pixels
   const PLAYERSHIP_CELLS_HEIGHT = 96;

   const PLAYERSHIP_START_X_POSITION = 380;
   const PLAYERSHIP_START_Y_POSITION = 300;

   const PLAYERSHIP_ANIMATION_FPS = 12;
   const PLAYERSHIP_HORIZONTAL_VELOCITY = 5;
   const PLAYERSHIP_VERTICAL_VELOCITY = 5;
   const PLAYERSHIP_MOVE_SPEED = 0.3;
   const PLAYERSHIP_HEALTH = 3;

   //How fast game scrolls down
   const BASE_GAME_FALL_SPEED =0.4;

   const PLAYERSHIP_CELLS_IDLE = [
      {  left: 1344, top: 0,
         width: PLAYERSHIP_CELLS_WIDTH, height: PLAYERSHIP_CELLS_HEIGHT },

      {  left: 1344, top: PLAYERSHIP_CELLS_HEIGHT,
         width: PLAYERSHIP_CELLS_WIDTH, height: PLAYERSHIP_CELLS_HEIGHT }
   ];

   const PLAYERSHIP_CELLS_LEFT = [
      { left: 1280, top: 0, 
        width: PLAYERSHIP_CELLS_WIDTH, height: PLAYERSHIP_CELLS_HEIGHT },

      { left: 1280, top: PLAYERSHIP_CELLS_HEIGHT, 
         width: PLAYERSHIP_CELLS_WIDTH, height: PLAYERSHIP_CELLS_HEIGHT }
   ];

   const PLAYERSHIP_CELLS_RIGHT = [
      { left: 1544, top: 0, 
        width: PLAYERSHIP_CELLS_WIDTH, height: PLAYERSHIP_CELLS_HEIGHT },

      { left: 1544, top: PLAYERSHIP_CELLS_HEIGHT, 
         width: PLAYERSHIP_CELLS_WIDTH, height: PLAYERSHIP_CELLS_HEIGHT }
   ];

   const PLAYERSHIP_CELLS_UP = [
      { left: 1480, top: 0, 
        width: PLAYERSHIP_CELLS_WIDTH, height: PLAYERSHIP_CELLS_HEIGHT },

      {  left: 1480, top: PLAYERSHIP_CELLS_HEIGHT, 
         width: PLAYERSHIP_CELLS_WIDTH, height: PLAYERSHIP_CELLS_HEIGHT }
   ];

   const PLAYERSHIP_CELLS_DOWN = [
      { left: 1412, top: 0, 
        width: PLAYERSHIP_CELLS_WIDTH, height: PLAYERSHIP_CELLS_HEIGHT },

      { left: 1412, top: PLAYERSHIP_CELLS_HEIGHT, 
         width: PLAYERSHIP_CELLS_WIDTH, height: PLAYERSHIP_CELLS_HEIGHT }
   ];

//End Region

//Region Bullets

   const BULLET_ALL_INITIAL_TRANSLATION = new Vector2(0, -100);

   //properties of BULLET_PLAYER
   const BULLET_PLAYER_MOVE_SPEED = 1;
   const BULLET_PLAYER_DIRECTION = new Vector2(0, -1);
   const BULLET_PLAYER_INTERVAL_IN_MS = 600;
   const BULLET_PLAYER_WIDTH = 20;
   const BULLET_PLAYER_HEIGHT = 52;
   const BULLET_PLAYER_ANIMATION_FPS = 12;
   const BULLET_PLAYER_SOUND = "sound_shoot";
   const BULLET_PLAYER_OFFSET = new Vector2(
      (PLAYERSHIP_CELLS_WIDTH/2) - (BULLET_PLAYER_WIDTH/2),
      -BULLET_PLAYER_HEIGHT
   );

   const BULLET_PLAYER_CELLS = [
      {  left: 984, top: 72,
         width: BULLET_PLAYER_WIDTH, height: BULLET_PLAYER_HEIGHT },
      
      {  left: 1040, top: 72,
         width: BULLET_PLAYER_WIDTH, height: BULLET_PLAYER_HEIGHT },
   ];

   //properties of POWERSHOT
   const BULLET_POWERSHOT_MOVE_SPEED = 0.2;
   const BULLET_POWERSHOT_DIRECTION = new Vector2(0, -1);
   const BULLET_POWERSHOT_INTERVAL_IN_MS = 200;
   const BULLET_POWERSHOT_WIDTH = 20;
   const BULLET_POWERSHOT_HEIGHT = 52;
   const BULLET_POWERSHOT_ANIMATION_FPS = 12;
   const BULLET_POWERSHOT_SOUND = "sound_shoot";
   const BULLET_POWERSHOT_OFFSET = new Vector2(
      (PLAYERSHIP_CELLS_WIDTH/2) - (BULLET_POWERSHOT_WIDTH/2),
      -BULLET_POWERSHOT_HEIGHT
   );

   const BULLET_POWERSHOT_CELLS = [
      {  left: 984, top: 72,
         width: BULLET_POWERSHOT_WIDTH, height: BULLET_POWERSHOT_HEIGHT },
      
      {  left: 1040, top: 72,
         width: BULLET_POWERSHOT_WIDTH, height: BULLET_POWERSHOT_HEIGHT },
   ];


   //properties of SPREADSHOT
   const BULLET_SPREADSHOT_MOVE_SPEED = 1;
   const BULLET_SPREADSHOT_DIRECTION = new Vector2(0, -1);
   const BULLET_SPREADSHOT_INTERVAL_IN_MS = 600;
   const BULLET_SPREADSHOT_WIDTH = 20;
   const BULLET_SPREADSHOT_HEIGHT = 20;
   const BULLET_SPREADSHOT_ANIMATION_FPS = 12;
   const BULLET_SPREADSHOT_SOUND = "sound_shoot";
   const BULLET_SPREADSHOT_OFFSET = new Vector2(
      (PLAYERSHIP_CELLS_WIDTH/2) - (BULLET_SPREADSHOT_WIDTH/2),
      -BULLET_POWERSHOT_HEIGHT
   );

   const BULLET_SPREADSHOT_CELLS = [
      {  left: 984, top: 28,
         width: BULLET_SPREADSHOT_WIDTH, height: BULLET_SPREADSHOT_HEIGHT },
      
      {  left: 1040, top: 28,
         width: BULLET_SPREADSHOT_WIDTH, height: BULLET_SPREADSHOT_HEIGHT }
   ];

   
   
   //properties of ENEMY_BULLET
   const BULLET_ENEMY_MOVE_SPEED = 0.4;
   const BULLET_ENEMY_DIRECTION = new Vector2(0, 1);
   const BULLET_ENEMY_FIRE_INTERVAL_IN_MS = 1200;
   const BULLET_ENEMY_WIDTH = 20;
   const BULLET_ENEMY_HEIGHT = 20;
   const BULLET_ENEMY_ANIMATION_FPS = 12;

   const BULLET_ENEMY_CELLS = [
      {  left: 984, top: 28,
         width: BULLET_ENEMY_WIDTH, height: BULLET_ENEMY_HEIGHT },
      
      {  left: 1040, top: 28,
         width: BULLET_ENEMY_WIDTH, height: BULLET_ENEMY_HEIGHT }
   ];

//End region 

//Region Asteroid
   const ASTEROID_CELLS_WIDTH = 96;
   const ASTEROID_CELLS_HEIGHT = 106;
   const ASTEROID_CELLS_START_X = 3;
   const ASTEROID_CELLS_START_Y = 2;
   const ASTEROID_CELLS_DISTANCE_ACCROSS = 128;
   const ASTEROID_CELLS_DISTANCE_DOWN = 128;
   const ASTEROID_ANIMATION_FPS = 14;

   const ASTEROID_BASE_FALL_SPEED = 3;
   const ASTEROID_HEALTH = 1;

   let asteroids_init_array = [];
   for(let i = 0; i < 4; i++)
   {
      for(let j = 0; j < 8; j++)
      {
         asteroids_init_array[(i*8) + j] = {
            left: ASTEROID_CELLS_START_X + (ASTEROID_CELLS_DISTANCE_ACCROSS * j),
            top: ASTEROID_CELLS_START_Y + (ASTEROID_CELLS_DISTANCE_DOWN * i),
            width: ASTEROID_CELLS_WIDTH, height: ASTEROID_CELLS_HEIGHT }
      }
   }
   const ASTEROID_CELLS = asteroids_init_array;

//End Region

//Region PowerShot Pickup
   const POWERSHOT_PICKUP_CELLS_WIDTH = 64;
   const POWERSHOT_PICKUP_CELLS_HEIGHT = 64;
   const POWERSHOT_PICKUP_START_X = 1087;
   const POWERSHOT_PICKUP_START_Y = 0;
   const POWERSHOT_PICKUP_CELLS_DISTANCE_ACROSS = 64;
   const POWERSHOT_PICKUP_ANIMATION_FPS = 16;

   const POWERSHOT_PICKUP_BASE_FALL_SPEED = 6;
   const POWERSHOT_PICKUP_HEALTH = 1;

   const POWERSHOT_PICKUP_CELLS =
   [
      {  left: POWERSHOT_PICKUP_START_X,   top: POWERSHOT_PICKUP_START_Y,
         width: POWERSHOT_PICKUP_CELLS_WIDTH, height: POWERSHOT_PICKUP_CELLS_HEIGHT },

      {  left: POWERSHOT_PICKUP_START_X + POWERSHOT_PICKUP_CELLS_DISTANCE_ACROSS,   top: POWERSHOT_PICKUP_START_Y,
         width: POWERSHOT_PICKUP_CELLS_WIDTH, height: POWERSHOT_PICKUP_CELLS_HEIGHT },
   ];

//End Region

//Region SpreadShot Pickup
   const SPREADSHOT_PICKUP_CELLS_WIDTH = 64;
   const SPREADSHOT_PICKUP_CELLS_HEIGHT = 64;
   const SPREADSHOT_PICKUP_START_X = 1087;
   const SPREADSHOT_PICKUP_START_Y = 64;
   const SPREADSHOT_PICKUP_CELLS_DISTANCE_ACROSS = 64;
   const SPREADSHOT_PICKUP_ANIMATION_FPS = 16;

   const SPREADSHOT_PICKUP_BASE_FALL_SPEED = 6;

   const SPREADSHOT_PICKUP_CELLS =
   [
      {  left: SPREADSHOT_PICKUP_START_X,   top: SPREADSHOT_PICKUP_START_Y,
         width: SPREADSHOT_PICKUP_CELLS_WIDTH, height: SPREADSHOT_PICKUP_CELLS_HEIGHT },

      {  left: SPREADSHOT_PICKUP_START_X + SPREADSHOT_PICKUP_CELLS_DISTANCE_ACROSS,   top: SPREADSHOT_PICKUP_START_Y,
         width: SPREADSHOT_PICKUP_CELLS_WIDTH, height: SPREADSHOT_PICKUP_CELLS_HEIGHT }
   ];

//End Region

//Region Health Pickup

   const HEALTH_PICKUP_CELLS_WIDTH = 64;
   const HEALTH_PICKUP_CELLS_HEIGHT = 64;
   const HEALTH_PICKUP_START_X = 1087;
   const HEALTH_PICKUP_START_Y = 128;
   const HEALTH_PICKUP_CELLS_DISTANCE_ACROSS = 64;
   const HEALTH_PICKUP_ANIMATION_FPS = 16;

   const HEALTH_PICKUP_BASE_FALL_SPEED = 6;

   const HEALTH_PICKUP_CELLS =
   [
      {  left: HEALTH_PICKUP_START_X,   top: HEALTH_PICKUP_START_Y,
         width: HEALTH_PICKUP_CELLS_WIDTH, height: HEALTH_PICKUP_CELLS_HEIGHT },

      {  left: HEALTH_PICKUP_START_X + HEALTH_PICKUP_CELLS_DISTANCE_ACROSS,   top: HEALTH_PICKUP_START_Y,
         width: HEALTH_PICKUP_CELLS_WIDTH, height: HEALTH_PICKUP_CELLS_HEIGHT },
   ];

//End Region

//Region Backgrounds

   const BACKGROUND_CELL = [
      { left: 0,   top: 590, width: 1104, height: 400 }
   ];

   const BACKGROUND_DATA = [
      {
        id: "background_1",
        spriteSheet: document.getElementById("spacerush_space_background"),
        sourcePosition: new Vector2(0, 0),
        sourceDimensions: new Vector2(900, 960),
        translation: new Vector2(0,0),
        rotation: 0,
        scale: new Vector2(1,1),
        origin: new Vector2(0,0),
        actorType: ActorType.Background,
        layerDepth: 0,
        scrollSpeedMultiplier: BASE_GAME_FALL_SPEED
      }
   ];

//End Region

//Region Enemie Ships

   //BigEnemy
   const BIG_SHIP_CELLS_WIDTH = 104;
   const BIG_SHIP_CELLS_HEIGHT = 120;
   const BIG_SHIP_CELLS_START_X = 12;
   const BIG_SHIP_CELLS_START_Y = 8;
   const BIG_SHIP_CELLS_DISTANCE_ACCROSS = 128;
   const BIG_SHIP_ANIMATION_FPS = 12;

   const BIG_SHIP_BASE_FALL_SPEED = 2;
   const BIG_SHIP_HEALTH = 3;
   const BIG_SHIP_HEIGHT_STOP = 200;
   const BIG_SHIP_SIDE_SCROLL_SPEED = 5;
      
   const BIG_SHIP_CELLS =
   [
      {  left: BIG_SHIP_CELLS_START_X,   top: BIG_SHIP_CELLS_START_Y,
         width: BIG_SHIP_CELLS_WIDTH, height: BIG_SHIP_CELLS_HEIGHT },

      {  left: BIG_SHIP_CELLS_START_X + BIG_SHIP_CELLS_DISTANCE_ACCROSS,   top: BIG_SHIP_CELLS_START_Y,
         width: BIG_SHIP_CELLS_WIDTH, height: BIG_SHIP_CELLS_HEIGHT }
   ];

   //MediumEnemy
   const MID_SHIP_CELLS_WIDTH = 128;
   const MID_SHIP_CELLS_HEIGHT = 64;
   const MID_SHIP_CELLS_START_X = 256;
   const MID_SHIP_CELLS_START_Y = 0;
   const MID_SHIP_CELLS_DISTANCE_ACCROSS = 128;
   const MID_SHIP_ANIMATION_FPS = 12;
   const MID_SHIP_BASE_FALL_SPEED = 4;
   
   const MID_SHIP_HEALTH = 3;

   const MID_SHIP_HEIGHT_STOP = 400;
   const MID_SHIP_SIDE_SCROLL_SPEED = 2;

   const MID_SHIP_CELLS =
   [
      {  left: MID_SHIP_CELLS_START_X,   top: MID_SHIP_CELLS_START_Y,
         width: MID_SHIP_CELLS_WIDTH, height: MID_SHIP_CELLS_HEIGHT },

      {  left: MID_SHIP_CELLS_START_X + MID_SHIP_CELLS_DISTANCE_ACCROSS,   top: MID_SHIP_CELLS_START_Y,
         width: MID_SHIP_CELLS_WIDTH, height: MID_SHIP_CELLS_HEIGHT }
   ];

   //small enemy
   const SMALL_SHIP_CELLS_WIDTH = 64;
   const SMALL_SHIP_CELLS_HEIGHT = 64;
   const SMALL_SHIP_CELLS_START_X = 511;
   const SMALL_SHIP_CELLS_START_Y = 0;
   const SMALL_SHIP_CELLS_DISTANCE_ACCROSS = 64;
   const SMALL_SHIP_ANIMATION_FPS = 12;
   const SMALL_SHIP_BASE_FALL_SPEED = 5;

   const SMALL_SHIP_HEALTH = 1;

   const SMALL_SHIP_CELLS =
   [
      {  left: SMALL_SHIP_CELLS_START_X, top: SMALL_SHIP_CELLS_START_Y,
         width: SMALL_SHIP_CELLS_WIDTH, height: SMALL_SHIP_CELLS_HEIGHT },

      {  left: SMALL_SHIP_CELLS_START_X + SMALL_SHIP_CELLS_DISTANCE_ACCROSS,   top: SMALL_SHIP_CELLS_START_Y,
         width: SMALL_SHIP_CELLS_WIDTH, height: SMALL_SHIP_CELLS_HEIGHT }
   ];

//End Region

//Region Effects
   //explosions
   const EXPLOSION_CELLS_WIDTH = 64;
   const EXPLOSION_CELLS_HEIGHT = 60;
   const EXPLOSION_CELLS_START_X = 640;
   const EXPLOSION_CELLS_START_Y = 0;
   const EXPLOSION_CELLS_DISTANCE_ACCROSS = 64;
   const EXPLOSION_ANIMATION_FPS = 12;
   
   init_array = [];
   for(let i = 0; i < 5; i++)
   {
      init_array[i] = {
      left: EXPLOSION_CELLS_START_X + (EXPLOSION_CELLS_DISTANCE_ACCROSS * i),
      top: EXPLOSION_CELLS_START_Y,
      width: EXPLOSION_CELLS_WIDTH, height: EXPLOSION_CELLS_HEIGHT }
   }
   const EXPLOSION_CELLS = init_array;

//End Regions