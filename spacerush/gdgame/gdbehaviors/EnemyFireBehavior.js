class EnemyFireBehavior{

    constructor(objectManager, bulletSprite, fireInterval, parent)
    {
        this.objectManager = objectManager;
        this.bulletSprite = bulletSprite;
        this.fireInterval = fireInterval; 
        this.timeSinceLastInMs = 0;
        this.bulletOffset = new Vector2(
            (parent.Artist.Cells[0].width/2) - (BULLET_ENEMY_WIDTH/2),
            BULLET_ENEMY_HEIGHT
        );
      
    }

    Execute(gameTime, parent)
    {
        this.HandleShoot(gameTime, parent);
    }

    HandleShoot(gameTime, parent)
    {
        this.timeSinceLastInMs += gameTime.ElapsedTimeInMs;

        //make or clone a bullet
        if(this.timeSinceLastInMs >= this.fireInterval)
        {
            let bulletClone = this.bulletSprite.Clone();
            
            bulletClone.AttachBehavior(
                new MoveBehavior(BULLET_ENEMY_DIRECTION, BULLET_ENEMY_MOVE_SPEED)
            );

            //set the position of the bullet
            bulletClone.Transform2D.Translation = parent.Transform2D.Translation.Clone();

            bulletClone.Transform2D.TranslateBy(this.bulletOffset);

            //enable the bullet
            bulletClone.StatusType = StatusType.IsUpdated | StatusType.IsDrawn;

            //add bullet to object manager
            this.objectManager.Add(bulletClone);
            
            //reset
            this.timeSinceLastInMs = 0;
        }
    }

    //#endregion

    //#region Common Methods - Equals, ToString, Clone
    Equals(other) {
        //to do...  
        throw "Not Yet Implemented";
    }

    ToString() {
        //to do...
        throw "Not Yet Implemented";
    }


    Clone() {
        //to do...
        throw "Not Yet Implemented";

    }
    //#endregion
}